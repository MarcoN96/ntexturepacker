# Binary Texture Packing for Monogame

Merges sprites into single texture and creates json document with sprite data.


# Usage

		TexturePacker tp = new TexturePacker();
            var settings = new TexturePackerSettings();
            settings.FolderPath = "Content//Sprites//";
            settings.GraphicsDevice = GraphicsDevice;
            settings.MaxTextureSize = 4096;

            if (settings.CreateTexture == true)
            {
                SpritesheetMgr = tp.Pack(settings);

                SpritesheetMgr.SaveAllAsPNG("Content//");
                SpritesheetMgr.SaveRegions("Content//spritesheetdata");
            }