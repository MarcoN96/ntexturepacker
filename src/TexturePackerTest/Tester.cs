﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NTexturePacker;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TexturePackerTest
{
    class Tester : Game
    {
        private GraphicsDeviceManager _gdm;
        private TexturePackerSettings _settings;
        private TexturePacker _packer;
        private string _resultDir = Directory.GetCurrentDirectory() + "//TestResult//";

        public Tester()
        {
            _gdm = new GraphicsDeviceManager(this);
            _settings = new TexturePackerSettings();
            _packer = new TexturePacker();
        }

        protected override void Initialize()
        {
            _settings.GraphicsDevice = _gdm.GraphicsDevice;
            _settings.FolderPath = Directory.GetCurrentDirectory() + "//TestData//";
            base.Initialize();
        }

        protected override void Update(GameTime gameTime)
        {
            Console.WriteLine("Start packing");

            var startTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            if (_settings.CreateTexture == true)
            {
                var spritesheetMgr = _packer.Pack(_settings);

                if (!Directory.Exists(_resultDir))
                {
                    Directory.CreateDirectory(_resultDir);
                }

                spritesheetMgr.SaveAllAsPNG(_resultDir);
                spritesheetMgr.SaveRegions(_resultDir);
            }

            var endTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            Console.WriteLine("Finished packing in " + System.Math.Abs(startTime - endTime) + "ms");

            base.Update(gameTime);
            this.Exit();
        }
    }
}
