﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NTexturePacker;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TexturePackerTest
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var tester = new Tester())
                tester.Run();

            Console.WriteLine("Test done. Press Any key to continue.");
            Console.Read();
        }
    }
}
