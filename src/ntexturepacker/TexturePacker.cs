﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTexturePacker
{
    public class TexturePacker
    {
        List<TextureInfo> TextureList = new List<TextureInfo>();

        List<Bin> BinList = new List<Bin>();
        List<Texture2D> SpriteSheetList = new List<Texture2D>();

        Texture2D SpriteSheetTexture;
        Bin binRoot;

        bool couldNotContainAll = true;

        public SpritesheetManager Pack(TexturePackerSettings settings)
        {
            SpritesheetManager sm = new SpritesheetManager();

#if DEBUG
            Console.WriteLine("Loading files");
            var startTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
#endif

            LoadImages(settings);

#if DEBUG
            var endTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            Console.WriteLine("Finished loading files in " + System.Math.Abs(startTime - endTime) + "ms");
#endif

            // SortImages(settings);

            while(couldNotContainAll)
            {
#if DEBUG
                Console.WriteLine("Creating sheet");
                startTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
#endif

                couldNotContainAll = false;

                CreateAndAddSheet(settings, ref sm);

#if DEBUG
                endTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                Console.WriteLine("Finished creating sheet in " + System.Math.Abs(startTime - endTime) + "ms");
#endif
            }

            return sm;
        }

        void CreateAndAddSheet(TexturePackerSettings settings, ref SpritesheetManager mgr)
        {
            SpriteSheetTexture = new Texture2D(settings.GraphicsDevice, settings.MaxTextureSize, settings.MaxTextureSize);
            binRoot = new Bin(new Rectangle(0, 0, settings.MaxTextureSize, settings.MaxTextureSize));

            PackImages(settings);

            if(settings.CreateTexture)
            {
                SaveToTexture(settings);
            }

            var spritesheet = CreateSpritesheet(settings);

            mgr.AddSpritesheet(spritesheet);
        }

        void LoadImages(TexturePackerSettings settings)
        {
            if(settings.IncludeSubfolders)
            {
                LoadFilesRecursive(settings, settings.FolderPath);
            }
            else
            {
                LoadFilesFromFolder(settings, settings.FolderPath);
            }
        }

        void LoadFilesRecursive(TexturePackerSettings settings, string path)
        {
            LoadFilesFromFolder(settings, path);

            var subdirectoryEntries = Directory.GetDirectories(path).ToList();

            Parallel.ForEach(subdirectoryEntries, entry =>
            {
                LoadFilesRecursive(settings, entry);
            });
        }

        void LoadFilesFromFolder(TexturePackerSettings settings, string path)
        {
            var files = Directory.GetFiles(path, settings.FileExtension).ToList();
            Parallel.ForEach(files, entry =>
            {
                TextureInfo ti = new TextureInfo();
                ti.Texture = LoadTexture(settings, entry);
                ti.Dimensions = new Vector2(ti.Texture.Width, ti.Texture.Height);
                ti.Area = (int)(ti.Dimensions.X * ti.Dimensions.Y);

                string fileName = GetFileNameFromPath(entry);
                ti.FileName = fileName;

                TextureList.Add(ti);
            });
        }

        void SortImages(TexturePackerSettings settings)
        {
            TextureList.Sort(new AreaComparer());
        }

        void PackImages(TexturePackerSettings settings)
        {
            foreach(var entry in TextureList)
            {
                if(entry.WasAdded == true)
                {
                    continue;
                }

                Bin bin = FindBin(binRoot, entry.Dimensions);
                if(bin != null)
                {
                    bin.InsertData(entry);
                    entry.WasAdded = true;
                }
                else
                {
                    couldNotContainAll = true;
                }
            }
        }

        void SaveToTexture(TexturePackerSettings settings)
        {
            BlitOntoSpritesheet(binRoot);
        }

        Spritesheet CreateSpritesheet(TexturePackerSettings settings)
        {
            Spritesheet spritesheet = new Spritesheet();

            spritesheet.Texture = SpriteSheetTexture;

            AddRegionData(binRoot, ref spritesheet.Regions);

            return spritesheet;
        }

        void AddRegionData(Bin bin, ref Dictionary<string, Rectangle> dictionary)
        {
            if(bin.IsEmpty())
            {
                return;
            }

            dictionary[bin.Data.FileName] = bin.OccupiedSpace;

            foreach(var child in bin.Children)
            {
                AddRegionData(child, ref dictionary);
            }
        }

        void BlitOntoSpritesheet(Bin bin)
        {
            if(bin.IsEmpty())
            {
                return;
            }

            Color[] colorData = new Color[bin.Data.Texture.Width * bin.Data.Texture.Height];
            bin.Data.Texture.GetData<Color>(colorData);

            SpriteSheetTexture.SetData<Color>(0, bin.OccupiedSpace, colorData, 0, colorData.Length);

            foreach(var child in bin.Children)
            {
                BlitOntoSpritesheet(child);
            }
        }

        Bin FindBin(Bin bin, Vector2 dimensionToContain)
        {
            if(bin.IsEmpty() && bin.CanContain(dimensionToContain))
            {
                return bin;
            }
            else
            {
                if(bin.Children.Count == 0)
                {
                    return null;
                }

                foreach(var child in bin.Children)
                {
                    Bin result = FindBin(child, dimensionToContain);
                    if(result != null)
                    {
                        return result;
                    }
                }
            }

            return null;
        }

        string GetFileNameFromPath(string path)
        {
            string fileName = Path.GetFileName(path);
            int index = fileName.IndexOf('.');
            if(index > 0)
            {
                fileName = fileName.Substring(0, index);
            }
            return fileName;
        }

        Texture2D LoadTexture(TexturePackerSettings settings, string file)
        {
            FileStream fileStream = new FileStream(file, FileMode.Open);
            Texture2D newSprite = Texture2D.FromStream(settings.GraphicsDevice, fileStream);
            fileStream.Dispose();
            return newSprite;
        }
    }
}
