﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NTexturePacker
{
    public class SpritesheetManager
    {
        public List<Spritesheet> SpritesheetList = new List<Spritesheet>();
        public string SpritesheetFileName = "Spritesheet";

        public SpritesheetManager()
        {

        }

        public Spritesheet GetSpritesheet(string spriteName)
        {
            foreach(var entry in SpritesheetList)
            {
                if(entry.Contains(spriteName))
                {
                    return entry;
                }
            }
            return null;
        }

        public Rectangle? GetRectangle(string spriteName)
        {
            foreach(var entry in SpritesheetList)
            {
                if(entry.Contains(spriteName))
                {
                    return entry.GetRegion(spriteName);
                }
            }
            return null;
        }

        public void AddSpritesheet(Spritesheet spritesheet)
        {
            SpritesheetList.Add(spritesheet);
        }

        public void SaveAllAsPNG(string path)
        {
#if DEBUG
            Console.WriteLine("Saving texture files");
            var startTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
#endif

            int i = -1;
            Parallel.ForEach(SpritesheetList, entry =>
            {
                var j = Interlocked.Increment(ref i);
                entry.SaveAsPNG(path + SpritesheetFileName + j + ".png");
            });

#if DEBUG
            var endTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            Console.WriteLine("Finished saving texture files in " + System.Math.Abs(startTime - endTime) + "ms");
#endif
        }

        public void SaveRegions(string path)
        {
#if DEBUG
            Console.WriteLine("Saving region files");
            var startTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
#endif

            int i = -1;
            Parallel.ForEach(SpritesheetList, entry =>
            {
                var j = Interlocked.Increment(ref i);
                var jsonData = JsonConvert.SerializeObject(entry);
                File.WriteAllText(path + j.ToString(), jsonData);
            });

#if DEBUG
            var endTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            Console.WriteLine("Finished saving region files in " + System.Math.Abs(startTime - endTime) + "ms");

            Console.WriteLine("Converting colorspace");
            startTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
#endif

            Parallel.ForEach(SpritesheetList, entry =>
            {
                Color[] data = new Color[entry.Texture.Width * entry.Texture.Height];
                entry.Texture.GetData(data);
                for(int j = 0; j != data.Length; j++)
                    data[j] = Color.FromNonPremultiplied(data[j].ToVector4());
                entry.Texture.SetData(data);
            });

#if DEBUG
            endTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            Console.WriteLine("Finished converting colorspace in " + System.Math.Abs(startTime - endTime) + "ms");
#endif
        }

        public void LoadRegions(string path, int count)
        {
            for(int i = 0; i < count; i++)
            {
                string jsonData = File.ReadAllText(path + i.ToString());

                var result = JsonConvert.DeserializeObject<Spritesheet>(jsonData);
                SpritesheetList.Add(result);
            }
        }
    }
}
