﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTexturePacker
{
    public class AreaComparer : IComparer<TextureInfo>
    {
        public int Compare(TextureInfo a, TextureInfo b)
        {
            if (a.Area == 0 || b.Area == 0)
            {
                return 0;
            }

            // CompareTo() method 
            return -a.Area.CompareTo(b.Area); // from biggest to smallest

        }
    }
}
