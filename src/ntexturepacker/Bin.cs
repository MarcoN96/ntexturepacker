﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTexturePacker
{
    public class Bin
    {
        public Rectangle Rect;

        public List<Bin> Children = new List<Bin>();

        public TextureInfo Data;
        public Rectangle OccupiedSpace;

        public Bin(Rectangle rect)
        {
            Rect = rect;
        }

        public bool CanContain(Vector2 dimensions)
        {
            if(dimensions.X <= Rect.Width &&
                dimensions.Y <= Rect.Height)
            {
                return true;
            }

            return false;
        }

        public bool IsEmpty()
        {
            if(Data == null)
            {
                return true;
            }

            return false;
        }

        public void InsertData(TextureInfo data)
        {
            Data = data;

            OccupiedSpace = new Rectangle(Rect.X, Rect.Y, /*Rect.X + */(int)Data.Dimensions.X, /*Rect.Y + */(int)Data.Dimensions.Y);

 
            //bin1 to the right, fills all height
            //bin2 to the down
            if (Data.Dimensions.X < Rect.Width)
            {
                Rectangle newBin1 = new Rectangle(Rect.X + (int)Data.Dimensions.X, Rect.Y, Rect.Width - (int)data.Dimensions.X, Rect.Height);
                Bin b = new Bin(newBin1);
                Children.Add(b);
            }
            if (Data.Dimensions.Y < Rect.Height)
            {
                Rectangle newBin2 = new Rectangle(Rect.X, Rect.Y + (int)data.Dimensions.Y, (int)data.Dimensions.X, Rect.Height - (int)data.Dimensions.Y);
                Bin b = new Bin(newBin2);
                Children.Add(b);
            }
        }
    }
}
